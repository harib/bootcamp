//
//  Contact.h
//  Contacts
//
//  Created by kumaran on 23/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property (strong) NSString *firstName;
@property (strong) NSString *lastName;

@property int age;

-(id) initWithFirstName:(NSString *) fname WithLastName:(NSString *)lname WithAge:(int) ageVal;
@end
