//
//  ContactDetailViewController.m
//  Contacts
//
//  Created by kumaran on 23/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "ContactDetailViewController.h"

@interface ContactDetailViewController ()

@end

@implementation ContactDetailViewController

@synthesize firstNameLabel, lastNameLabel, ageLabel, contact;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.firstNameLabel.text = [contact firstName];
    self.lastNameLabel.text = [contact lastName];
    self.ageLabel.text = [NSString stringWithFormat:@"%d", [contact age]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
