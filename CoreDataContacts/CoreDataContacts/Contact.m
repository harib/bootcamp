//
//  Contact.m
//  CoreDataContacts
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import "Contact.h"


@implementation Contact

@dynamic firstName;
@dynamic lastName;
@dynamic company;
@dynamic mobile;
@dynamic homePhone;
@dynamic workPhone;
@dynamic homeEmail;
@dynamic workEmail;
@dynamic twitterHandle;
@dynamic birthday;

@end
