//
//  BootCampTests.m
//  BootCampTests
//
//  Created by Hariharan Thiagarajan on 05/23/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import "BootCampTests.h"
#import "KVCModel.h"
#import "KVCController.h"
#import "PModel.h"
#import "PController.h"



@implementation BootCampTests

- (void)setUp {
    [super setUp];

    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.

    [super tearDown];
}

- (void) testProtocols {
    PModel *model = [[PModel alloc] init];
    PController *controller = [[PController alloc] init];

    model.delegate = controller;

    model.name = @"Its awesome iOS";
}

- (void)testKVC {
    KVCModel *model = [[KVCModel alloc] init];
    KVCController *controller = [[KVCController alloc] init];

    controller.model = model;
    [controller registerAnObserver];
    model.name = @"iOS";
    model.name = @"Android";
    model.place = @"Bangalore";
}

@end
