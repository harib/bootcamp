//
//  TWViewController.m
//  NewsTable
//
//  Created by Hariharan Thiagarajan on 05/24/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import "TWViewController.h"
#import "SBJsonParser.h"

@interface TWViewController ()
@property NSMutableData *response;

@end

@implementation TWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)getData:(id)sender {
    NSString *url = @"https://search.twitter.com/search.json?q=thoughtworks&rpp=5&include_entities=true&result_type=mixed";
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:url]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [connection start];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {



    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSError *error = nil;

    id dataFromTweets = [parser objectWithData:data];
    NSLog(@"Data received %@",dataFromTweets);
}



/*
 + (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage
 {
 NSURL *url = [NSURL URLWithString:urlString];
 
 dispatch_queue_t callerQueue = dispatch_get_current_queue();
 dispatch_queue_t downloadQueue = dispatch_queue_create("com.myapp.processsmagequeue", NULL);
 dispatch_async(downloadQueue, ^{
 NSData * imageData = [NSData dataWithContentsOfURL:url];
 
 dispatch_async(callerQueue, ^{
 processImage(imageData);
 });
 });
 dispatch_release(downloadQueue);
 }

*/
@end