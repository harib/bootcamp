//
//  TWViewController.m
//  ObjectBoot
//
//  Created by Hariharan Thiagarajan on 05/24/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import "TWViewController.h"

@interface TWViewController ()
@property (weak, nonatomic) IBOutlet UILabel *firstName;
@property (weak, nonatomic) IBOutlet UILabel *lastName;

@end

@implementation TWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end