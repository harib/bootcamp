//
//  main.m
//  ObjectBoot
//
//  Created by Hariharan Thiagarajan on 05/24/13.
//  Copyright (c) 2013 TW. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TWAppDelegate.h"
#import "MyClass.h"

int main(int argc, char *argv[])
{
    NSString *greetings = @"Hello";
    int i = 10;
    NSNumber *number = @10.1234;
    MyClass *myClass = [[MyClass alloc] init];
    NSLog(@"%@", myClass);
    NSMutableArray  *array = @[@"hello", @"world", @"foo", @"bar", @12, myClass];


    NSMutableDictionary *dictionary = [NSDictionary dictionaryWithObjects:
            @[@"obj1", @1, [NSArray arrayWithObjects:@"hello", nil]]
                                                           forKeys:@[@"k1", @"k2", @"k3"]];
    NSLog(@"Dictionary : %@", dictionary);
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWAppDelegate class]));
    }
}