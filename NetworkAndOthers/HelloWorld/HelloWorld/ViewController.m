//
//  ViewController.m
//  HelloWorld
//
//  Created by Hariharan Thiagarajan on 24/05/13.
//  Copyright (c) 2013 HTech. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *greetingsLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameText;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)greetUser:(id)sender {
    self.greetingsLabel.text = [NSString stringWithFormat:@"Hello %@!", self.nameText.text];
}

-(IBAction)viewTouched:(id)sender {
    NSLog(@"View Touced");
    [self.nameText resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
