//
//  ViewController.h
//  JSONReader
//
//  Created by kumaran on 24/05/13.
//  Copyright (c) 2013 kumaran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong) NSMutableData *responseData;
@property (strong) NSString *responseString;

@end
